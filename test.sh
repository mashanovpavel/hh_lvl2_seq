#!/bin/bash

JAVA="java";
JAVA_RUN="${JAVA} -cp out/production/hh_seq/"
GEN="${JAVA_RUN} ru/h32/hh/seq/Main";
GEN_PARAMS="10000 10000 100"
SIMPLEST_SOLVER="${JAVA_RUN} ru/h32/hh/seq/Main simplest";
SOLVER="${JAVA_RUN} ru/h32/hh/seq/Main long";
INPUT_SEQ=$1

if [ ! ${INPUT_SEQ} -gt 0 ]; then
 echo "Use test.sh <seq>";
 exit;
fi

cleanup() {
    rm -f simplest.out.txt
    rm -f long.out.txt
}

run_simplest() {
    ${SIMPLEST_SOLVER} ${INPUT_SEQ} > simplest.out.txt
}

run_testing_solver() {
    ${SOLVER} ${INPUT_SEQ} > long.out.txt
}

test_equality() {
    SIMPLEST=`cat simplest.out.txt`
    TESTING=`cat long.out.txt`

    if [ "${SIMPLEST}" == "${TESTING}" ];
    then
        echo "${i} OK ${SIMPLEST}=${TESTING}"
    else
        echo "Error!"
        echo "Simplest=" ${SIMPLEST}
        echo "Solver  =" ${TESTING}
        exit;
    fi
}

#=========================================

for i in `seq 1 1000`;
    do
        echo -n "${i}..."

        INPUT_SEQ=${i}
        cleanup;
        run_simplest;
        run_testing_solver;
        test_equality;
    done

