package ru.h32.hh.seq.solvers;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;

public class SimpleSolver implements SeqSolver {

    protected String search = "";
    protected byte[] searchBytes = null;

    @Override
    public String getPositionOf(String in) {

        this.search = in;
        this.searchBytes = fromString(in);

        BigInteger iterator = new BigInteger("1");
        BigInteger one = new BigInteger("1");
        BigInteger acc = new BigInteger("1");


        for (int i = 1; i < 10000; i++) {
            iterator = iterator.add(one);

            int digits = (int) Math.ceil(log10(iterator)) + 1;

            //Сдвиг на ширину итератора
            acc = acc.multiply(BigInteger.TEN.pow(digits));

            //Запись в хвост
            acc = acc.add(iterator);

            byte[] current = fromString(acc.toString());

            printArr(current);

            //Оставим только самый длинный хвост, совпадающий в носом искомой последовательности
            current = getTail(current);


//            int index = current.indexOf(search);
//            if (index != -1) {
//
//
//                return String.valueOf(index + 1);
//            }
        }

//        System.out.println(acc.toString());

        return "not found";
    }

    protected byte[] getTail(byte[] current) {

        int fromIndex = 0;

        for(int i = current.length -1 ; i >= 0; i--)
        {
            boolean allRight = true;
            int to = Math.min(i, searchBytes.length);
            int j=0;
            for(j = 0; allRight && j < to; j++)
            {
                if(current[i-j] != this.searchBytes[searchBytes.length-j])
                {
                    allRight = false;
                }
            }

            if(allRight && j == searchBytes.length)
            {
                //нашли
            }
            else
            {

            }
        }

//        return Arrays.copyOfRange();
    }

    protected static byte[] fromString(String s) {
        String[] chars = s.split("");
        byte[] b = new byte[chars.length];

        for (int i = chars.length - 1; i >= 0; i--) {
            b[chars.length - i - 1] = Byte.parseByte(chars[i]);
        }

        return b;
    }

    protected static void printArr(byte[] ar) {
        for (int i = ar.length - 1; i >= 0; i--) {
            System.err.print(ar[i]);
        }

        System.err.println();
    }

    private int log10(BigInteger huge) {
        int digits = 0;
        int bits = huge.bitLength();
        // Serious reductions.
        while (bits > 4) {
            // 4 > log[2](10) so we should not reduce it too far.
            int reduce = bits / 4;
            // Divide by 10^reduce
            huge = huge.divide(BigInteger.TEN.pow(reduce));
            // Removed that many decimal digits.
            digits += reduce;
            // Recalculate bitLength
            bits = huge.bitLength();
        }
        // Now 4 bits or less - add 1 if necessary.
        if (huge.intValue() > 9) {
            digits += 1;
        }
        return digits;
    }
}
