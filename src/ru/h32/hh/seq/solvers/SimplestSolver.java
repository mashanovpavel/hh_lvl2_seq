package ru.h32.hh.seq.solvers;

import ru.h32.hh.seq.solvers.SeqSolver;

import java.math.BigInteger;

public class SimplestSolver implements SeqSolver {

    protected String search = "";

    @Override
    public String getPositionOf(String in) {

        this.search = in;

        BigInteger iterator = new BigInteger("1");
        BigInteger one = new BigInteger("1");
        BigInteger acc = new BigInteger("1");


        for (int i = 1; i < 10000; i++) {
            iterator = iterator.add(one);

            int digits = (int) Math.ceil(log10(iterator)) + 1;

            //Сдвиг на ширину итератора
            acc = acc.multiply(BigInteger.TEN.pow(digits));

            //Запись в хвост
            acc = acc.add(iterator);

            String current = acc.toString();

            //Оставим только самый длинный хвост, совпадающий в носом искомой последовательности


            int index = current.indexOf(search);
            if(index != -1)
            {


                return String.valueOf(index+1);
            }
        }

//        System.out.println(acc.toString());

        return "not found";
    }

    //    protected static byte[] fromString(String s)
//    {
//        String[] chars = s.split("");
//        byte[] b = new byte[chars.length];
//
//        for(int i = chars.length-1; i >= 0; i--)
//        {
//            b[chars.length-i-1] = Byte.parseByte(chars[i]);
//        }
//
//        return b;
//    }

    private int log10(BigInteger huge) {
        int digits = 0;
        int bits = huge.bitLength();
        // Serious reductions.
        while (bits > 4) {
            // 4 > log[2](10) so we should not reduce it too far.
            int reduce = bits / 4;
            // Divide by 10^reduce
            huge = huge.divide(BigInteger.TEN.pow(reduce));
            // Removed that many decimal digits.
            digits += reduce;
            // Recalculate bitLength
            bits = huge.bitLength();
        }
        // Now 4 bits or less - add 1 if necessary.
        if ( huge.intValue() > 9 ) {
            digits += 1;
        }
        return digits;
    }
}
