//package ru.h32.hh.seq.solvers;
//
//import java.util.Arrays;
//import java.util.Vector;
//
//public class LongMathSolver implements SeqSolver{
//
//    protected byte[] iterator = null;
//    protected byte[] accumulator = null;
//    protected byte[] tail = null;
////    protected byte[] position = null;
//
//    protected int iteratorLen = 1;
//    protected int accumulatorLen = 1;
//    protected int tailLen = 1;
//    protected int positionLen = 1;
//
//    protected int tailPos = 0;
//    protected long position = 0;
//
//    protected String searchString = "";
//    protected byte[] search = null;
//
//    @Override
//    public String getPositionOf(String in) {
//
//        search = fromString(in);
//
//        //init
//        iteratorLen = 1;
//        iterator = new byte[10];
//
//        tailLen = 0;
//        tail = new byte[search.length];
//
//        tailPos = 0;
//        position = 1;
//
//        boolean found = false;
//        for(int i = 0; i < 1000000; i++)
//        {
//            incrementIterator();
////            printArr(iterator, iteratorLen);
//            //Найти нужное в хвосте+интераторе.
//            found = searchInTailAndIterator();
//
//            if(found)
//                return String.valueOf(position);
//
//        }
//
//        return "";
//    }
//
//    protected boolean searchInTailAndIterator1() {
//
//
//        return false;
//    }
//
//    /**
//     * Увеличивает на 1 значение
//     *
//     * @return номер старшего измененного разряда
//     */
//    protected int incrementIterator()
//    {
//        int max = 0;
//
//        iterator[0] += 1;
//
//        for(int i = 0; i < iteratorLen; i++)
//        {
//            if(iterator[i] >= 10)
//            {
//                //Перенос
//                max = i;
//
//                iterator[i] = (byte) (iterator[i] % 10);
//
//                if((i+1) < iteratorLen) //Умещается
//                {
//                    iterator[i+1] += 1;
//                } else {//Не умещается
//                    if(iteratorLen+1 < iterator.length) {
//                        iteratorLen++;
//                    } else {
//                        iterator = grow(iterator);
//                        iteratorLen++;
//                    }
//                    iterator[i+1] += 1;
//                }
//            }
//        }
//
//        return max;
//    }
//
//    protected static byte[] grow(byte[] array)
//    {
//        return Arrays.copyOf(array, array.length*2);
//    }
//
//    protected static byte[] fromString(String s)
//    {
//        String[] chars = s.split("");
//        byte[] b = new byte[chars.length];
//
//        for(int i = chars.length-1; i >= 0; i--)
//        {
//            b[chars.length-i-1] = Byte.parseByte(chars[i]);
//        }
//
//        return b;
//    }
//
//    protected static void printArr(byte[] ar, int effectiveLen)
//    {
////        System.err.print("Len:" + effectiveLen + "\n");
//
//        for(int i = ar.length-1; i >= 0; i--)
//        {
//            if(i+1 == effectiveLen) {
//                System.err.print("~");
//            }
//            System.err.print(ar[i]);
//        }
//
//        System.err.println();
//    }
//}
