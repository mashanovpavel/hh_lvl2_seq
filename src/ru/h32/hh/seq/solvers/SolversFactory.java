package ru.h32.hh.seq.solvers;

public class SolversFactory {

    public static SeqSolver getSolver(String solver) throws SolverNotFoundException {
        if(solver.equals("simplest"))
        {
            return new SimplestSolver();
        }else if(solver.equals("simple"))
        {
            return new SimpleSolver();
        }

        throw new SolverNotFoundException();
    }

}
