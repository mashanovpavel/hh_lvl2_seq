package ru.h32.hh.seq.solvers;

public interface SeqSolver {

    public String getPositionOf(String in);

}
