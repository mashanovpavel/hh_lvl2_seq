package ru.h32.hh.seq;

import ru.h32.hh.seq.solvers.SeqSolver;

import java.util.Arrays;

public class Seq implements SeqSolver {

    protected byte[] search = null;
    protected byte[] iterator = null;
    protected byte[] accumulator = null;

    protected int iteratorLength = 0;
    protected int accumulatorLength = 0;

    public String getPositionOf(String in) {

        search = fromString(in);
        iterator = new byte[search.length];
        accumulator = new byte[search.length];

        printLongNumber(search);

        Nose n = new Nose();
        n.len = search.length;
        n.digits = new byte[search.length];

        Iter iter = new Iter();
        iter.len = 0;
        iter.digits = new byte[0];

        while (!longEquals(accumulator, search)) {
            longAddOne(iterator);
        }

        return "-1";
    }

    protected void longShiftWithLong(byte[] acc, int accLength, byte[] shifter, int shifterLength)
    {

    }

    protected static byte[] fromString(String s)
    {
        System.out.println(s);
        String[] chars = s.split("");
        byte[] b = new byte[chars.length];

        for(int i = chars.length-1; i >= 0; i--)
        {
            b[chars.length-i-1] = Byte.parseByte(chars[i]);
        }

        return b;
    }

    /**
     * Добавляет число add к длинному числу acc
     *
     * @param acc
     */
    protected static void longAddOne(byte[] acc)
    {
        boolean moved = false;
        acc[0]+=1;
//
//        for(int i = 0; i < acc.length; i++) {
//            if(add[i] > 9) {
//
//                moved = true;
//
//                add[i] = (byte) ((byte) add[i] % 10);
//
//                if( i < add.length - 1) {
//                    add[i+1] += 1;
//                }
//            }
//
//            if(!moved)
//                return;
//        }
    }

    /**
     * Добавляет длинное число add к длинному числу acc
     *
     * @param acc
     * @param add
     */
    protected static void longAddLong(byte[] acc, byte[] add)
    {
        for(int i = 0; i < add.length; i++) {
            add[i] += add[i];

            if(add[i] > 9) {
               add[i] = (byte) ((byte) add[i] % 10);

                if( i < add.length - 1) {
                    add[i+1] += 1;
                }
            }
        }
    }

    protected static boolean longEquals(byte[] l1, byte[] l2)
    {
        return Arrays.equals(l1, l2);
    }

    protected static void printLongNumber(byte[] number)
    {
        for(int i = number.length-1; i >= 0; i--)
        {
            System.out.print(number[i]);
        }
    }

}

class Nose {
    public int len = 0;
    public byte[] digits = null;

    public void enlargeTo(int size)
    {

    }

}

class Iter {
    public int len = 0;
    public byte[] digits = null;
}


