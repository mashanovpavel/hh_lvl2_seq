package ru.h32.hh.seq;

import ru.h32.hh.seq.solvers.SeqSolver;
import ru.h32.hh.seq.solvers.SolverNotFoundException;
import ru.h32.hh.seq.solvers.SolversFactory;

public class Main {

    public static void main(String[] args) {
        if(args.length == 2)
        {
            SeqSolver solver = null;
            try {
                solver = SolversFactory.getSolver(args[0]);
            } catch (SolverNotFoundException e) {
                System.out.println("Solver not found. Use param: <solver> <sequence>");
            }

            String result =  solver.getPositionOf(args[1]);

            System.out.println(result);
        }
        else
        {
            System.out.println("Required param: <solver> <sequence>");
        }
    }

}
